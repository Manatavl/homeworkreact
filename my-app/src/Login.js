import { useState, useEffect } from 'react';
import axios from 'axios';
import './Login.css';
import App from './App';

function sendLogin(email, passw) {
    console.log('email=' + email);
    console.log('passw=' + passw);
    fetch('https://jsonplaceholder.typicode.com/posts', {
        type: 'POST',
        data: {email, passw} 
        })
    .then(response => response.json())
    .then((data) => {console.log(data);});
    
    window.location.assign('/App.js'); 
    
    // axios.get('https://api.npms.io/v2/search?q=react')
    //    .then(response => this.setState({ totalReactPackages: response.data.total }));
  }

function Login() {
    const [email, setemail] = useState('');
    const [passw, setpassw] = useState('');
  return (
    <div className="block">
      <h1>Вход в систему</h1>
      <div className="block">
        <span className="input100">email: </span>
        <input
          className="input100"
          name="email"
          onChange={(event) => setemail(event.target.value)}
        />
      </div>
      <div className="block">
        password:&nbsp;&nbsp;&nbsp;&nbsp;
        <input
          className="input100"
          name="passw"
          onChange={(event) => setpassw(event.target.value)}
        />
      </div>
      <div className="container-login100-form-btn">
        <button
          className="login100-form-btn"
          onClick={() => sendLogin(email, passw)}
        >
          Войти
        </button>
      </div>
    </div>
  );
}

export default Login;
